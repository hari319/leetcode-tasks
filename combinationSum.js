function combinationSum(candidates, target) {
    candidates.sort(function (a, b) { return a - b; });
    var length = candidates.length;
    var res = [];
    search(0, [], target);
    return res;

    function search(id, prefix, target) {
        if (target === 0)
            res.push(prefix.slice());
        if (id === length)
            return;
        if (target <= 0)
            return;
        prefix.push(candidates[id]);
        search(id, prefix, target - candidates[id]);
        prefix.pop();
        search(id + 1, prefix, target);
    }
};

console.log(combinationSum([2, 3, 5], 8));
console.log(combinationSum([2], 1));
console.log(combinationSum([1], 1));
console.log(combinationSum([1], 2));
