function combinationSum(candidates: number[], target: number): number[][] {
    candidates.sort((a, b) => a - b);
    const res: number[][] = [];
    findNumbers(0, [], target);
    return res;
    
    function findNumbers(id: number, prefix: number[], target: number){
        if(target === 0) res.push(prefix.slice())
        if(id === candidates.length) return;
        if(target <= 0) return;
        prefix.push(candidates[id])
        findNumbers(id, prefix, target - candidates[id])
        prefix.pop()
        findNumbers(id +1, prefix, target)
    }
};


console.log(combinationSum( [2,3,5], 8))