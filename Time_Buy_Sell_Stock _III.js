function maxProfit(prices) {
    if(prices.length == 0) return 0
    let dp = new Array(prices.length).fill(0);
    for(let t = 1; t <= 2; t++){ 
        let min = prices[0];
        let max = 0;
        for(let i = 1; i < prices.length; i++){
            min = Math.min(min, prices[i] - dp[i]);
            max = Math.max(max, prices[i] - min);
            dp[i] = max;
        }
    }
    return dp.pop();
}

console.log(maxProfit([3,3,5,0,0,3,1,4]));
console.log(maxProfit([1,2,3,4,5]));
console.log(maxProfit([7,6,4,3,1]));
console.log(maxProfit([1]));