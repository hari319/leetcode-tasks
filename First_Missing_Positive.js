function firstMissingPositive(nums){    
    for (let i = 1; i <= 300; i++){
        let found = nums.includes(i)
        if(!found) { 
            return i
        }
    }
};

console.log(firstMissingPositive([1,2,0]))
console.log(firstMissingPositive([3,4,-1,1]))
console.log(firstMissingPositive([7,8,9,11,12])