function searchRange(nums, target) {
    var result = [];
    var defaultValue = [-1, -1];
    nums.forEach(function (item, i) {
        if (item === target) {
            result.push(i);
        }
    });
    if (result.length === 0) {
        result = defaultValue;
    }
    return result;
}
;
console.log(searchRange([5, 7, 8, 8, 8, 10], 8));
